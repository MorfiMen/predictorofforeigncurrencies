# PredictorOfForeignCurrencies

This is a little challenge, to make a prediction of currency exchange between, Colombian peso (COP) and American Dollar (USD) 
using the following parameters: Inflation, CPI (consumer price indexes), PPI (President Popularity Index) of Colombia, 
AMP (Average Mayor Popularity) of Colombia, External Debt and RRM (representative exchange rate of the market).

All of this using Machine Learning in this case I use Deep Learning to make the model.

## Technologies

*  Python to make the model and predictor.
*  .NET with C# using to make microservices for the backend.