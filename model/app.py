import requests
from flask import Flask, request, jsonify, send_from_directory
app = Flask(__name__)
import pandas as pd
import quandl
import math
import random
import os
import numpy as np
from sklearn import preprocessing, cross_validation, svm
from sklearn.linear_model import LinearRegression
from keras.models import load_model
from sklearn.preprocessing import MinMaxScaler

@app.route('/getdata/', methods=['GET'])
def getData():
	df = pd.read_csv('data/data.csv')
	data = df.sort_index(ascending=True, axis = 0)
	new_data = pd.DataFrame(index=range(0, len(f)), columns=['Date', 'RRM'])

	for i in range(0, len(data)):
	    new_data['Date'][i] = data['Date'][i]
	    new_data['RRM'][i] = data['RRM'][i]
	
	new_data.index = new_data.Date
	new_data.drop('Date', axis = 1, inplace = True)

	bottom_value = request.args.get('date1')
	up_value = request.args.get('date2')

	model = load_model('/trained_model/model.h5')

	scaler = MinMaxScaler(feature_range=(0, 1))

	predict_data = model.predict(new_input)
	predict_data = scaler.inverse_transform(predcit_data)

	valid['Predictions'] = predict_data
	valid = valid.rename(columns={'Date':'USD Price'})
	valid = valid.to_json(orient='table')


	return jsonify(valid)
